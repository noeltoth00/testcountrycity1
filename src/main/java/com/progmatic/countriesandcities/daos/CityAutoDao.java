/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.countriesandcities.daos;

import com.progmatic.countriesandcities.entities.City;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author peti
 */
public interface CityAutoDao extends JpaRepository<City, String>{
    List<City> findByName(String cityName);
    
    
}
