/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.countriesandcities.daos;

import com.progmatic.countriesandcities.entities.Country;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author peti
 */
public interface CountryAutoDao extends JpaRepository<Country, String> {
    // List<Country> findByCountryId(String iso);
    //Country findByCountryId(String iso);
}
