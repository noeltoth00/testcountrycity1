/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.countriesandcities.controllers;

import com.progmatic.countriesandcities.dtos.CityDto;
import com.progmatic.countriesandcities.dtos.CountryDto;
import com.progmatic.countriesandcities.entities.Country;
import com.progmatic.countriesandcities.services.CCService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author peti
 */
@Controller
public class CC_Controller {

    @Autowired
    CCService cCService;

   

    @RequestMapping(path = {"/countries","/"}, method = RequestMethod.GET)
    public String showCountries(Model m) {
        List<CountryDto> allCountries = cCService.listAllCountries();
        m.addAttribute("countryList", allCountries);
        return "countries";
    }

    //TODO check this, seems a bit buggy
    @RequestMapping(path = "/cities", method = RequestMethod.GET)
    public String showCities(Model m) {
        List<CityDto> allCities = cCService.listAllCities();
        m.addAttribute("cityList", allCities);
        return "cities";
    }

    @RequestMapping(path = "/country/{iso}", method = RequestMethod.GET)
    public String showOneCountry(
            @PathVariable("iso") String countryId,
            Model m) {
        CountryDto cData = cCService.detailedCountryData(countryId);
        m.addAttribute("countryData", cData);
        return "/countryDetail";
    }

}
